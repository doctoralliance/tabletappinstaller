﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Security.Principal;
using System.Windows.Forms;
using SharpAdbClient;

namespace TabletAppInstaller
{
    public partial class InstallApp : Form
    {
        adbManager adbManager;

        DeviceData selectedDevice = new DeviceData();

        public bool IsUserAdministrator()
        {
            var isAdmin = false;
            try
            {
                WindowsIdentity user = WindowsIdentity.GetCurrent();
                WindowsPrincipal principal = new WindowsPrincipal(user);
                isAdmin = principal.IsInRole(WindowsBuiltInRole.Administrator);
            }
            catch (UnauthorizedAccessException ex)
            {
                showStatusMessage(ex.Message);

                isAdmin = false;
            }
            catch (Exception ex)
            {
                showStatusMessage(ex.Message);

                isAdmin = false;
            }

            return isAdmin;
        }

        public InstallApp()
        {
            InitializeComponent();

            if (!IsUserAdministrator())
            {
                showStatusMessage("Please run this application as windows administrator.");

                InstallAPKButton.Hide();
            }
        }

        private void InstallApp_Load(object sender, EventArgs e)
        {
            rtextBoxInstallationStatus.Text = "";
            rtextBoxInstallationStatus.ScrollBars = RichTextBoxScrollBars.Vertical;
            gridViewConnectedDeviceList.ScrollBars = ScrollBars.Vertical;

            adbManager = new adbManager();

            try
            {
                adbManager.StartServer();
            }
            catch (Exception ex)
            {
                showStatusMessage(ex.Message);

                InstallAPKButton.Hide();
            }

            GetListOfDevices();
        }

        private void GetListOfDevices()
        {
            rtextBoxInstallationStatus.Text = "";

            var deviceData = adbManager.ListAllConnectedDevices();

            if (deviceData.Count < 1)
            {
                showStatusMessage("Could not get list of devices. Please connect and try again.");

                InstallAPKButton.Hide();
            }

            try
            {
                bindGridViewToShowDevices(deviceData);

                InstallAPKButton.Show();
            }
            catch (Exception ex)
            {
                showStatusMessage(ex.Message);
                InstallAPKButton.Hide();
            }
        }

        private void bindGridViewToShowDevices(List<DeviceData> deviceData)
        {
            gridViewConnectedDeviceList.DataSource = null;
            gridViewConnectedDeviceList.Columns.Clear();
            gridViewConnectedDeviceList.Rows.Clear();
            gridViewConnectedDeviceList.Refresh();

            DataGridViewCheckBoxColumn col = new DataGridViewCheckBoxColumn(false);
            col.Name = "Select";
            gridViewConnectedDeviceList.Columns.Add(col);

            gridViewConnectedDeviceList.DataSource = deviceData;

        }

        private void gridViewConnectedDeviceList_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {

        }

        private void gridViewConnectedDeviceList_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {

        }

        private void gridViewConnectedDeviceList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (gridViewConnectedDeviceList.CurrentCell.ColumnIndex == 0)
            {
                if (gridViewConnectedDeviceList.CurrentCell != null)
                {
                    bool checkstate = (bool)gridViewConnectedDeviceList.CurrentCell.GetEditedFormattedValue(e.RowIndex, DataGridViewDataErrorContexts.Commit);

                    if (checkstate)
                    {
                        var deviceSerial = gridViewConnectedDeviceList[e.ColumnIndex + 1, e.RowIndex] as DataGridViewCell;
                        var deviceName = gridViewConnectedDeviceList[e.ColumnIndex + 2, e.RowIndex] as DataGridViewCell;

                        var deviceSerialValue = deviceSerial.FormattedValue.ToString();
                        var deviceNameValue = deviceName.FormattedValue.ToString();

                        SelectedDeviceTextBox.Text = deviceSerialValue;

                        selectedDevice.Serial = deviceSerialValue;
                        selectedDevice.Name = deviceNameValue;

                        var deviceInfo = gridViewConnectedDeviceList[e.ColumnIndex + 3, e.RowIndex] as DataGridViewCell;
                        selectedDevice.Model = deviceInfo.FormattedValue.ToString();

                        deviceInfo = gridViewConnectedDeviceList[e.ColumnIndex + 4, e.RowIndex] as DataGridViewCell;
                        selectedDevice.Product = deviceInfo.FormattedValue.ToString();

                        deviceInfo = gridViewConnectedDeviceList[e.ColumnIndex + 5, e.RowIndex] as DataGridViewCell;
                        selectedDevice.State = deviceInfo.FormattedValue.ToString().ToLower() == "online" ? DeviceState.Online : DeviceState.Offline;

                        showStatusMessage("Selected device serial is " + deviceSerialValue + " and state is " + selectedDevice.State.ToString());

                        if (selectedDevice.State != DeviceState.Online)
                        {
                            showStatusMessage("Selected device is " + selectedDevice.State.ToString() + ". \n " + "Please make sure your device is online to install the application.");
                        }
                    }
                }
            }
        }

        private void BrowseAPKFileButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            DialogResult dr = ofd.ShowDialog();
            if (dr == DialogResult.OK)
            {
                var userSelectedFilePath = ofd.FileName;

                SelectAPKTextBox.Text = userSelectedFilePath;

                InstallAPKButton.Show();

                showStatusMessage("You have selected the APK file to install.");
            }
        }

        private void Refresh_Click(object sender, EventArgs e)
        {
            SelectedDeviceTextBox.Text = string.Empty;

            GetListOfDevices();

            showStatusMessage("Refreshed the list of devices available.");
        }

        private void InstallAPKButton_Click(object sender, EventArgs e)
        {
            Directory.SetCurrentDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ADB"));
            if (string.IsNullOrWhiteSpace(selectedDevice.Serial))
            {
                // alert message that say select a device
                showStatusMessage("Please select a device to install.");
                return;
            }

            if (string.IsNullOrWhiteSpace(SelectAPKTextBox.Text) || !SelectAPKTextBox.Text.EndsWith("apk"))
            {
                // alert message that say select apk file
                showStatusMessage("Please select a valid APK file");
                return;
            }

            var adbFullPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ADB", "adb.exe");

            // set non market apps settings
            try
            {
                var status = ExecuteAdbCommand(adbFullPath, "shell settings put secure install_non_market_apps 1");

                if (!string.IsNullOrWhiteSpace(status))
                {
                    showStatusMessage("Status: \n " + status);
                }
                else
                {
                    showStatusMessage("Configured tablet to accept new installation. Proceeding to install application. \n");
                }
            }
            catch (Exception ex)
            {
                showStatusMessage(ex.Message);

                return;
            }

            var installationSuccess = false;

            try
            {
                var apkFilePath = new FileInfo(SelectAPKTextBox.Text);

                installationSuccess = adbManager.InstallPackage(selectedDevice, apkFilePath.FullName, true);

                if (installationSuccess)
                {
                    showStatusMessage("Application installed successfully. Now making this app as admin of the device. \n");

                    try
                    {
                        var status = ExecuteAdbCommand(adbFullPath, "shell dpm set-device-owner com.doctoralliance.tabletdelivery/.Receiver.AdminReceiver");

                        if (!string.IsNullOrWhiteSpace(status))
                        {
                            showStatusMessage("Status: \n" + status + "\n Please reboot this device.");
                        }
                        else
                        {
                            showStatusMessage("Command executed successfully on the selected device. Now the App is ready and pinned in the device. \n Please reboot this device.");
                        }
                    }
                    catch (Exception ex)
                    {
                        showStatusMessage(ex.Message);
                    }
                }
                else
                {
                    showStatusMessage("Failed to install doctoralliance application. Package installation is not completed.");
                }
            }
            catch (Exception ex)
            {
                showStatusMessage(ex.Message);
            }
        }

        public string ExecuteAdbCommand(string adbPath, string command)
        {
            try
            {
                System.Diagnostics.ProcessStartInfo procStartInfo =
                    new System.Diagnostics.ProcessStartInfo("cmd", "/c " + adbPath + " " + command);

                procStartInfo.RedirectStandardOutput = true;
                procStartInfo.UseShellExecute = false;
                procStartInfo.CreateNoWindow = true;
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo = procStartInfo;
                proc.Start();
                string result = proc.StandardOutput.ReadToEnd();
                return result;
            }
            catch (Exception objException)
            {
                //Console.WriteLine(objException.Message);
                return objException.Message;
            }
        }

        private void RebootDevice_Click(object sender, EventArgs e)
        {
            try
            {
                adbManager.Reboot(selectedDevice);
            }
            catch (Exception ex)
            {
                showStatusMessage(ex.Message);
            }
        }

        private void RestartInstallerApp_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void showStatusMessage(string messageToShow)
        {
            rtextBoxInstallationStatus.Text += messageToShow + " \n ";
        }
    }


    public class AndroidMultiLineReceiver : MultiLineReceiver
    {
        public List<string> Lines { get; set; }

        public AndroidMultiLineReceiver()
        {
            Lines = new List<String>();
        }

        protected void ProcessNewLines(string[] lines)
        {
            foreach (var line in lines)
                Lines.Add(line);
        }

        protected override void ProcessNewLines(IEnumerable<string> lines)
        {
            foreach (var line in lines)
                Lines.Add(line);
        }
    }
}
