﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using SharpAdbClient;
using SharpAdbClient.DeviceCommands;

namespace TabletAppInstaller
{
    public class adbManager
    {
        public void StartServer()
        {
            AdbServer server = new AdbServer();

            var adbExePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ADB", "adb.exe");

            //var adbExePath = @"..\..\ADB\adb.exe";

            if (File.Exists(adbExePath))
            {
                var result = server.StartServer(adbExePath, restartServerIfNewer: false);
            }
            else
            {
                throw new Exception("Could not find Adb.Exe Path, can not continue to install APK.");
            }
        }

        public List<DeviceData> ListAllConnectedDevices()
        {
            var devices = AdbClient.Instance.GetDevices();

            return devices;
        }

        void Test()
        {
            var monitor = new DeviceMonitor(new AdbSocket(new IPEndPoint(IPAddress.Loopback, AdbClient.AdbServerPort)));
            monitor.DeviceConnected += this.OnDeviceConnected;
            monitor.Start();
        }

        public void OnDeviceConnected(object sender, DeviceDataEventArgs e)
        {
            Console.WriteLine($"The device {e.Device.Name} has connected to this PC");
        }

        public void Reboot(string into, DeviceData device)
        {
            AdbClient.Instance.Reboot(into, device);
        }

        public void Reboot(DeviceData device)
        {
            AdbClient.Instance.Reboot(device);
        }

        public void SetDevice(IAdbSocket socket, DeviceData device)
        {
            AdbClient.Instance.SetDevice(socket, device);
        }

        public Task ExecuteRemoteCommandAsync(string command, DeviceData device, IShellOutputReceiver receiver, CancellationToken cancellationToken, int maxTimeToOutputResponse)
        {
            return AdbClient.Instance.ExecuteRemoteCommandAsync(command, device, receiver, cancellationToken, maxTimeToOutputResponse, AdbClient.Encoding);
        }

        public void Install(DeviceData device, Stream apk, params string[] arguments)
        {
            AdbClient.Instance.Install(device, apk, arguments);
        }

        public bool InstallPackage(DeviceData device, string apkPath, bool reinstall = false)
        {
            try
            {
                PackageManager manager = new PackageManager(device);

                manager.InstallPackage(apkPath, reinstall);

                return true;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public bool RunExeCommand(DeviceData device, string command, IShellOutputReceiver receiver)
        {
            try
            {                
                // ex: command: "adb shell dpm set-device-owner com.dfobo.cmx.dfoboappandroid/.AdminReceiver"

                AdbClient.Instance.ExecuteRemoteCommand(command, device, receiver);

                return true;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void Disconnect(DnsEndPoint endpoint)
        {
            AdbClient.Instance.Disconnect(endpoint);
        }
    }
}
