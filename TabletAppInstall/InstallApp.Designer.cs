﻿namespace TabletAppInstaller
{
    partial class InstallApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridViewConnectedDeviceList = new System.Windows.Forms.DataGridView();
            this.SelectAPKTextBox = new System.Windows.Forms.TextBox();
            this.BrowseAPKFileButton = new System.Windows.Forms.Button();
            this.RefreshButton = new System.Windows.Forms.Button();
            this.SelectedDeviceTextBox = new System.Windows.Forms.TextBox();
            this.APKFilePathLabel = new System.Windows.Forms.Label();
            this.SelectedDeviceLabel = new System.Windows.Forms.Label();
            this.InstallAPKButton = new System.Windows.Forms.Button();
            this.RebootDevice = new System.Windows.Forms.Button();
            this.RestartInstallerApp = new System.Windows.Forms.Button();
            this.rtextBoxInstallationStatus = new System.Windows.Forms.RichTextBox();
            this.installStatusLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewConnectedDeviceList)).BeginInit();
            this.SuspendLayout();
            // 
            // gridViewConnectedDeviceList
            // 
            this.gridViewConnectedDeviceList.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.gridViewConnectedDeviceList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridViewConnectedDeviceList.Location = new System.Drawing.Point(12, 12);
            this.gridViewConnectedDeviceList.Name = "gridViewConnectedDeviceList";
            this.gridViewConnectedDeviceList.Size = new System.Drawing.Size(1324, 160);
            this.gridViewConnectedDeviceList.TabIndex = 0;
            this.gridViewConnectedDeviceList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridViewConnectedDeviceList_CellContentClick);
            this.gridViewConnectedDeviceList.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.gridViewConnectedDeviceList_RowsAdded);
            // 
            // SelectAPKTextBox
            // 
            this.SelectAPKTextBox.Location = new System.Drawing.Point(174, 248);
            this.SelectAPKTextBox.Name = "SelectAPKTextBox";
            this.SelectAPKTextBox.Size = new System.Drawing.Size(435, 22);
            this.SelectAPKTextBox.TabIndex = 1;
            // 
            // BrowseAPKFileButton
            // 
            this.BrowseAPKFileButton.AutoSize = true;
            this.BrowseAPKFileButton.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.BrowseAPKFileButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BrowseAPKFileButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.BrowseAPKFileButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BrowseAPKFileButton.Location = new System.Drawing.Point(615, 248);
            this.BrowseAPKFileButton.Name = "BrowseAPKFileButton";
            this.BrowseAPKFileButton.Size = new System.Drawing.Size(136, 27);
            this.BrowseAPKFileButton.TabIndex = 2;
            this.BrowseAPKFileButton.Text = "Browse APK File";
            this.BrowseAPKFileButton.UseVisualStyleBackColor = false;
            this.BrowseAPKFileButton.Click += new System.EventHandler(this.BrowseAPKFileButton_Click);
            // 
            // RefreshButton
            // 
            this.RefreshButton.AutoSize = true;
            this.RefreshButton.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.RefreshButton.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.RefreshButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.RefreshButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RefreshButton.Location = new System.Drawing.Point(615, 178);
            this.RefreshButton.Name = "RefreshButton";
            this.RefreshButton.Size = new System.Drawing.Size(129, 27);
            this.RefreshButton.TabIndex = 3;
            this.RefreshButton.Text = "Refresh List";
            this.RefreshButton.UseVisualStyleBackColor = false;
            this.RefreshButton.Click += new System.EventHandler(this.Refresh_Click);
            // 
            // SelectedDeviceTextBox
            // 
            this.SelectedDeviceTextBox.Location = new System.Drawing.Point(174, 285);
            this.SelectedDeviceTextBox.Name = "SelectedDeviceTextBox";
            this.SelectedDeviceTextBox.ReadOnly = true;
            this.SelectedDeviceTextBox.Size = new System.Drawing.Size(435, 22);
            this.SelectedDeviceTextBox.TabIndex = 4;
            // 
            // APKFilePathLabel
            // 
            this.APKFilePathLabel.AutoSize = true;
            this.APKFilePathLabel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.APKFilePathLabel.Location = new System.Drawing.Point(15, 251);
            this.APKFilePathLabel.Name = "APKFilePathLabel";
            this.APKFilePathLabel.Size = new System.Drawing.Size(94, 17);
            this.APKFilePathLabel.TabIndex = 5;
            this.APKFilePathLabel.Text = "APK File Path";
            // 
            // SelectedDeviceLabel
            // 
            this.SelectedDeviceLabel.AutoSize = true;
            this.SelectedDeviceLabel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SelectedDeviceLabel.Location = new System.Drawing.Point(15, 290);
            this.SelectedDeviceLabel.Name = "SelectedDeviceLabel";
            this.SelectedDeviceLabel.Size = new System.Drawing.Size(110, 17);
            this.SelectedDeviceLabel.TabIndex = 6;
            this.SelectedDeviceLabel.Text = "Selected Device";
            // 
            // InstallAPKButton
            // 
            this.InstallAPKButton.AutoSize = true;
            this.InstallAPKButton.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.InstallAPKButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.InstallAPKButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightBlue;
            this.InstallAPKButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InstallAPKButton.Location = new System.Drawing.Point(174, 345);
            this.InstallAPKButton.Name = "InstallAPKButton";
            this.InstallAPKButton.Size = new System.Drawing.Size(435, 27);
            this.InstallAPKButton.TabIndex = 7;
            this.InstallAPKButton.Text = "Install APK";
            this.InstallAPKButton.UseVisualStyleBackColor = false;
            this.InstallAPKButton.Click += new System.EventHandler(this.InstallAPKButton_Click);
            // 
            // RebootDevice
            // 
            this.RebootDevice.AutoSize = true;
            this.RebootDevice.BackColor = System.Drawing.SystemColors.Info;
            this.RebootDevice.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.RebootDevice.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.RebootDevice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RebootDevice.Location = new System.Drawing.Point(615, 382);
            this.RebootDevice.Name = "RebootDevice";
            this.RebootDevice.Size = new System.Drawing.Size(129, 27);
            this.RebootDevice.TabIndex = 8;
            this.RebootDevice.Text = "Reboot Device";
            this.RebootDevice.UseVisualStyleBackColor = false;
            this.RebootDevice.Click += new System.EventHandler(this.RebootDevice_Click);
            // 
            // RestartInstallerApp
            // 
            this.RestartInstallerApp.AutoSize = true;
            this.RestartInstallerApp.BackColor = System.Drawing.SystemColors.Control;
            this.RestartInstallerApp.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.RestartInstallerApp.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.RestartInstallerApp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RestartInstallerApp.Location = new System.Drawing.Point(1169, 382);
            this.RestartInstallerApp.Name = "RestartInstallerApp";
            this.RestartInstallerApp.Size = new System.Drawing.Size(167, 27);
            this.RestartInstallerApp.TabIndex = 9;
            this.RestartInstallerApp.Text = "Restart Installer";
            this.RestartInstallerApp.UseVisualStyleBackColor = false;
            this.RestartInstallerApp.Click += new System.EventHandler(this.RestartInstallerApp_Click);
            // 
            // rtextBoxInstallationStatus
            // 
            this.rtextBoxInstallationStatus.Location = new System.Drawing.Point(835, 210);
            this.rtextBoxInstallationStatus.Name = "rtextBoxInstallationStatus";
            this.rtextBoxInstallationStatus.ReadOnly = true;
            this.rtextBoxInstallationStatus.Size = new System.Drawing.Size(501, 162);
            this.rtextBoxInstallationStatus.TabIndex = 10;
            this.rtextBoxInstallationStatus.Text = "";
            // 
            // installStatusLabel
            // 
            this.installStatusLabel.AutoSize = true;
            this.installStatusLabel.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.installStatusLabel.Location = new System.Drawing.Point(835, 183);
            this.installStatusLabel.Name = "installStatusLabel";
            this.installStatusLabel.Padding = new System.Windows.Forms.Padding(10, 5, 20, 5);
            this.installStatusLabel.Size = new System.Drawing.Size(149, 27);
            this.installStatusLabel.TabIndex = 11;
            this.installStatusLabel.Text = "Installation Status";
            // 
            // InstallApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1348, 420);
            this.Controls.Add(this.installStatusLabel);
            this.Controls.Add(this.rtextBoxInstallationStatus);
            this.Controls.Add(this.RestartInstallerApp);
            this.Controls.Add(this.RebootDevice);
            this.Controls.Add(this.InstallAPKButton);
            this.Controls.Add(this.SelectedDeviceLabel);
            this.Controls.Add(this.APKFilePathLabel);
            this.Controls.Add(this.SelectedDeviceTextBox);
            this.Controls.Add(this.RefreshButton);
            this.Controls.Add(this.BrowseAPKFileButton);
            this.Controls.Add(this.SelectAPKTextBox);
            this.Controls.Add(this.gridViewConnectedDeviceList);
            this.Name = "InstallApp";
            this.Text = "Tablet Delivery Installation";
            this.Load += new System.EventHandler(this.InstallApp_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewConnectedDeviceList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView gridViewConnectedDeviceList;
        private System.Windows.Forms.TextBox SelectAPKTextBox;
        private System.Windows.Forms.Button BrowseAPKFileButton;
        private System.Windows.Forms.Button RefreshButton;
        private System.Windows.Forms.TextBox SelectedDeviceTextBox;
        private System.Windows.Forms.Label APKFilePathLabel;
        private System.Windows.Forms.Label SelectedDeviceLabel;
        private System.Windows.Forms.Button InstallAPKButton;
        private System.Windows.Forms.Button RebootDevice;
        private System.Windows.Forms.Button RestartInstallerApp;
        private System.Windows.Forms.RichTextBox rtextBoxInstallationStatus;
        private System.Windows.Forms.Label installStatusLabel;
    }
}

